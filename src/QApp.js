import React, { useEffect, useState } from "react";
// import qrcode from 'qrcode.react';
import {
  CardHeader,
  CardBody,
  Button,
  Table,
  FormGroup,
  Label,
  Dropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu
} from "reactstrap";
import CheckForm from "./checkpointComponents/CheckForm";
import Check from "./checkpointComponents/Check";

const QApp = () => {
  const [checkpoints, setCheckpoints] = useState([]);
  const [showForm, setShowForm] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [checkpointToEdit, setCheckpointToEdit] = useState({});
  const [checkpointIndex, setCheckpointIndex] = useState(null);
  const [dropdownIsOpen, setDropdownIsOpen] = useState(false);
  // additional
  const [cities, setCities] = useState([]);
  const [selectedCity, setSelectedCity] = useState("");

  useEffect(() => {
    fetch(
      "https://coronavirus-ph-api.now.sh/mm-checkpoints?fbclid=IwAR1nmt9LkVw3YEsj8EllmWRbZjLEHOLtmoG0HnGFqaPp6OPRvpYTev9wkSA"
    )
      .then(res => res.json())
      .then(res => {
        setCheckpoints(res);
        // console.log(res)
        //additional
        let cities = [];
        //additional
        res.forEach(item => {
          if (!cities.includes(item.city)) {
            cities.push(item.city);
          }
        });
        //additional
        setCities(cities);
      });
  }, []);

  const deleteCheckpoint = index => {
    // console.log(index)

    const newCheckpoint = checkpoints.filter((checkpoint, qIndex) => {
      return qIndex != index;
    });

    setCheckpoints(newCheckpoint);
  };

  const saveCheckpoint = (district, city, location) => {
    // console.log(district)
    let newCheckpoints = [];

    //editing part
    if (isEditing) {
      let editedDistrict = district;
      let editedCity = city;
      let editedLocation = location;

      if (district === "") editedDistrict = checkpointToEdit.district;
      if (city === "") editedCity = checkpointToEdit.city;
      if (location === "") editedLocation = checkpointToEdit.location;

      newCheckpoints = checkpoints.map((checkpoint, index) => {
        if (index === checkpointIndex) {
          return {
            district: editedDistrict,
            city: editedCity,
            location: editedLocation
          };
        } else {
          return checkpoint;
        }
      });
    } else {
      newCheckpoints = [
        ...checkpoints,
        {
          district: district,
          city: city,
          location: location
        }
      ];
    }
    setCheckpoints(newCheckpoints);
    setShowForm(false);
    setIsEditing(false);
    setCheckpointToEdit({});
    setCheckpointIndex(null);
  };

  const editCheckpoint = (checkpoint, index) => {
    setShowForm(true);
    setIsEditing(true);
    setCheckpointToEdit(checkpoint);
    setCheckpointIndex(index);
  };

  const toggleForm = () => {
    setShowForm(false);
    setIsEditing(false);
    setCheckpointToEdit({});
  };
  //additional
  const filterCity = city => {
    setSelectedCity(city);
    fetch(
      "https://coronavirus-ph-api.now.sh/mm-checkpoints?fbclid=IwAR1nmt9LkVw3YEsj8EllmWRbZjLEHOLtmoG0HnGFqaPp6OPRvpYTev9wkSA"
    )
      .then(res => res.json())
      .then(res => {
        // setCheckpoints(res);
        // console.log(res)

        let checkpoints = res.filter(item => {
          return item.city === city;
        });
        setCheckpoints(checkpoints);
      });
  };
  return (
    <>
      <div>
        <CardHeader
          className="py-5"
          style={{
            backgroundImage:
              "linear-gradient(to right, #0d00e4, #c10707, #d4dd00 )",
            backgroundPosition: "center",
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat"
          }}
        >
          <h1 id="title" className="text-center text-light">
            Enhanced Quarantine Luzon Checkpoints
          </h1>
          <h6 className="text-center text-light" id="by">
            Made after washing hands 🧴🤲🏻 by Mabelle and Janica
          </h6>
        </CardHeader>
        <div className="d-flex justify-content-center p-3">
          <Button color="primary" onClick={() => setShowForm(!showForm)}>
            {" "}
            + Add Checkpoint
          </Button>
          <CheckForm
            showForm={showForm}
            toggleForm={toggleForm}
            saveCheckpoint={saveCheckpoint}
            isEditing={isEditing}
            checkpointToEdit={checkpointToEdit}
          />
        </div>
        {/* additional for dropdown */}
        <div className="d-flex justify-content-center">
          <FormGroup>
            <Dropdown
              isOpen={dropdownIsOpen}
              toggle={() => setDropdownIsOpen(!dropdownIsOpen)}
            >
              <DropdownToggle caret>
                {selectedCity === "" ? "Filter by City" : selectedCity}
              </DropdownToggle>
              <DropdownMenu>
                {cities.map((city, index) => (
                  <DropdownItem
                    key={index}
                    onClick={() => {
                      filterCity(city);
                    }}
                  >
                    {city}
                  </DropdownItem>
                ))}
              </DropdownMenu>
            </Dropdown>
          </FormGroup>
        </div>
        {/* end of additional dropdown */}
        <CardBody className="p-3">
          {/* questions */}
          <Table>
            <tbody>
              <Check
                checkpoints={checkpoints}
                deleteCheckpoint={deleteCheckpoint}
                toggleForm={editCheckpoint}
              />
            </tbody>
          </Table>
        </CardBody>
      </div>
    </>
  );
};

export default QApp;
