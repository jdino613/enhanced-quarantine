import React from "react";
import { Button } from "reactstrap";
// import pnp from "./pnp.png";

const Check = props => {
  return (
    <>
      {props.checkpoints.map((checkpoint, index) => (
        <tr key={index}>
          <td className="text-center">
            <p id="bold1">Checkpoint00{checkpoint.id}</p>
            <p
              className="text-danger"
              id="district"
              className="d-flex justify-content-center text-danger"
            >
              <p id="bold">District: &nbsp;</p> {checkpoint.district}
              &nbsp;
              <img
                style={{ marginLeft: "0.4rem" }}
                src="https://seeklogo.com/images/P/pnp-philippine-national-police-logo-555C0B67B2-seeklogo.com.png"
                id="pnp"
                alt="logo"
              />
            </p>
            <p
              className="text-secondary"
              className="d-flex justify-content-center"
              id="city"
            >
              <p id="bold">City: &nbsp;</p> {checkpoint.city}
            </p>
            <p
              className="text-secondary"
              id="location"
              className="d-flex justify-content-center"
            >
              {" "}
              <p id="bold"> Location: &nbsp;</p> {checkpoint.location}
            </p>
            <Button
              id="btn"
              color="danger"
              onClick={() => props.deleteCheckpoint(index)}
              className="my-2"
            >
              Delete
            </Button>{" "}
            <Button
              id="btn"
              color="warning"
              onClick={() => props.toggleForm(checkpoint, index)}
            >
              Edit
            </Button>
          </td>
        </tr>
      ))}
    </>
  );
};

export default Check;
