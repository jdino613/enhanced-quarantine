import React, { useState } from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  FormGroup,
  Label,
  Input,
  Button
} from "reactstrap";

const CheckForm = props => {
  const [district, setDistrict] = useState("");
  const [city, setCity] = useState("");
  const [location, setLocation] = useState("");

  const handleSaveCheckpoint = () => {
    props.saveCheckpoint(district, city, location);
    setDistrict("");
    setCity("");
    setLocation("");
  };
  return (
    <>
      <Modal id="modal" isOpen={props.showForm} toggle={props.toggleForm}>
        <ModalHeader
          toggle={props.toggleForm}
          id="modalheader"
          className="text-light"
        >
          <img
            id="logo"
            src="https://lh3.googleusercontent.com/proxy/lXSeYtk9mQhkIKWQ_DhuDgpbn2vRu2DoDIAX8RBvbtyBq-KF-x7M9seCI1B8QdjwygCnkwUBReQqls7L7fWlQ4dyH_r0hZNf7q96U3wv0pQbXb4rD56M-iMJsHmV_Q-NaxEymNf0x0O3RYw"
          />
          {props.isEditing ? "Edit Checkpoint" : "Add Checkpoint"}
        </ModalHeader>
        <ModalBody>
          <FormGroup>
            <Label id="modallabel">District:</Label>
            <Input
              placeholder="Enter District"
              onChange={e => setDistrict(e.target.value)}
              defaultValue={props.checkpointToEdit.district}
            />
          </FormGroup>
          <FormGroup>
            <Label id="modallabel">City:</Label>
            <Input
              placeholder="Enter City"
              onChange={e => setCity(e.target.value)}
              defaultValue={props.checkpointToEdit.city}
            />
          </FormGroup>
          <FormGroup>
            <Label id="modallabel">Location:</Label>
            <Input
              placeholder="Enter Location"
              onChange={e => setLocation(e.target.value)}
              defaultValue={props.checkpointToEdit.location}
            />
          </FormGroup>
          <Button id="modalbtn" color="success" onClick={handleSaveCheckpoint}>
            {props.isEditing ? "Edit Checkpoint" : "Add Checkpoint"}
          </Button>
        </ModalBody>
      </Modal>
    </>
  );
};

export default CheckForm;
